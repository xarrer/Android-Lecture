package com.example.dell.androidclass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private String tag = "AndroidClass: ";
    private EditText etInputOne, etInputTwo;
    private TextView tvTotal, tvTextOne;
    int valOne, valTwo, valTotal;
    Button btnAdd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        Log.i(tag, "I am inside oncreate() event");

    }

    private void init() {
        etInputOne = findViewById(R.id.etInputOne);
        etInputTwo = findViewById(R.id.etInputTwo);
        tvTotal = findViewById(R.id.tvTotal);
        Button btnAdd = findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValFromUser();
                setTotalVal();
                //new method to receive input
                //must parse to integer coz we get value in string form


            }
        });

    }

    private void getValFromUser() {
        valOne = Integer.parseInt(etInputOne.getText().toString());
        valTwo = Integer.parseInt(etInputTwo.getText().toString());
        valTotal = valOne + valTwo;
        if (etInputOne.getText().toString().equals("Advance") && etInputTwo.getText().toString().equals("123")) {
        } else if (etInputOne.getText().toString().equals("") || etInputTwo.getText().toString().equals("")) {
            Toast.makeText(this, "Error In Integer Number", Toast.LENGTH_SHORT).show();
        }

        private void setTotalVal() {
            tvTotal.setText("Total = " + valTotal);
        }
        /**
        @Override
        protected void onStart() {
            super.onStart();
            Log.i(tag, "I am on onStart() event");
        }
        @Override
        protected void onStop () {
            super.onStop();
            Log.i(tag, "I am on onStop() event");
        }
*/

    }
}